<?php

namespace Cherrypulp\LaravelWordpress\Tests;

use Cherrypulp\LaravelWordpress\Facades\LaravelWordpress;
use Cherrypulp\LaravelWordpress\ServiceProvider;
use Orchestra\Testbench\TestCase;

class LaravelWordpressTest extends TestCase
{
    protected function getPackageProviders($app)
    {
        return [ServiceProvider::class];
    }

    protected function getPackageAliases($app)
    {
        return [
            'laravel-wordpress' => LaravelWordpress::class,
        ];
    }

    public function testExample()
    {
        $this->assertEquals(1, 1);
    }
}
