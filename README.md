# Laravel Wordpress

[![Build Status](https://travis-ci.org/cherrypulp/laravel-wordpress.svg?branch=master)](https://travis-ci.org/cherrypulp/laravel-wordpress)
[![Scrutinizer Code Quality](https://scrutinizer-ci.com/gl/cherry-pulp/cherrypulp_libraries/laravel-wordpress/badges/quality-score.png?b=master&s=0bdc86095f73cf7dc620b03411108fb362483758)](https://scrutinizer-ci.com/gl/cherry-pulp/cherrypulp_libraries/laravel-wordpress/?branch=master)

[![Packagist](https://img.shields.io/packagist/v/cherrypulp/laravel-wordpress.svg)](https://packagist.org/packages/cherrypulp/laravel-wordpress)
[![Packagist](https://poser.pugx.org/cherrypulp/laravel-wordpress/d/total.svg)](https://packagist.org/packages/cherrypulp/laravel-wordpress)
[![Packagist](https://img.shields.io/packagist/l/cherrypulp/laravel-wordpress.svg)](https://packagist.org/packages/cherrypulp/laravel-wordpress)

Laravel helper for Wordpress integration into your project. It's a wrapper to implement 3 solutions : using direct database connection (using Corcel), using the RestAPI (using WP-Json) and using GraphQL Wordpress Plugin.

## Why having 3 solutions and not using only one ?

Corcel access directly to the WP Database to make queries that grant definitely a better db access for quering post but it doesn't render any native wordpress scripts/hooks like Gutenberg rendering or plugins filters [see issue](https://github.com/corcel/corcel/issues/479).

The strategy for that is to use the WpGraphQL Api (strongly typed) or Wp Rest Api (weakly typed) to allow special rendering through the Wordpress so you can have benefits from these 3 solutions in your Laravel Project.

## Installation

Install via composer

```bash
composer require cherrypulp/laravel-wordpress
```

### Register Service Provider

**Note! This and next step are optional if you use laravel>=5.5 with package
auto discovery feature.**

Add service provider to `config/app.php` in `providers` section
```php
Cherrypulp\LaravelWordpress\ServiceProvider::class,
```

### Register Facade

Register package facade in `config/app.php` in `aliases` section
```php
Cherrypulp\LaravelWordpress\Facades\LaravelWordpress::class,
```

### Publish Configuration File

```bash
php artisan vendor:publish --provider="Cherrypulp\LaravelWordpress\ServiceProvider" --tag="config"
```

### Setup the database

To make it work, you must set the database connection to your Wordpress and add https://github.com/wp-graphql/wp-graphql.

Then in your .env file you must define :

````bash
WP_GRAPHQL_URL="http://xxxx/wp/graphql"
WP_API_URL="http://xxxx/wp-json/"

# Important if you want to have access to protected endpoint
WP_API_TOKEN="http://xxxx/wp-json/" #Api Token @see https://developer.wordpress.org/rest-api/using-the-rest-api/authentication/#authentication-plugins on how to generate that
WP_API_TYPE="BEARER" #The type of Authentication used @see : https://developer.wordpress.org/rest-api/using-the-rest-api/authentication/#authentication-plugins for more information (default is Bearer Oauth type)
WP_DB_HOST="xxxx"
WP_DB_NAME="xxxx"
WP_DB_USER="xxxx"
WP_DB_PASSWORD="xxxx"
````

# Usage

This package is a Wrapper for Laravel Corcel and Wordpress GraphQL allowing direct connection to Wordpress.

Please check the documentation for more information [https://github.com/corcel/corcel](https://github.com/corcel/corcel),  [https://gitlab.com/cherrypulp/libraries/blok-graphql](https://gitlab.com/cherrypulp/libraries/blok-graphql) and [https://github.com/wp-graphql/wp-graphql](https://github.com/wp-graphql/wp-graphql) for more infos.

## GraphQL Based query

GraphQL client is based on our package : [https://gitlab.com/cherrypulp/libraries/blok-graphql](https://gitlab.com/cherrypulp/libraries/blok-graphql).

You can basically make just a query like that :

````php
$posts = lwp()->graphql()->query($graphqlQuery, $params);
````

### getPostBySlug

````php
$post = lwp()->getPostBySlug('hello-world');
````

Return the post by is slug

# Available methods

````php
$home = lwp()->getHome();

# Return the homepage information

$post = lwp()->getPostBySlug('hello-world');

$posts = lwp()->getHighlightedPosts();

# Get the posts marked as featured posts

$category = lwp()->getCategoryBySlug('essential');

lwp()->flush()->getHome();

# Flush all cached queries

````

## Using REST Api

You can access through :

```php
$posts = lwp()->http()->get('wp/v2/posts');
```

Under the hood it's just a wrapper on top of HTTP client of Laravel : [https://laravel.com/docs/8.x/http-client#introduction](https://laravel.com/docs/8.x/http-client#introduction)

## Using Corcel models

Corcel models are accessible through lwp()->{$modelName} variable.

Exemple : the model Post is accessible through lwp()->post->slug('hello-world');

For more infos, see : [https://github.com/corcel/corcel](https://github.com/corcel/corcel)

# Getting deeper

## How to override GraphQL query ?

You can override default GraphQL query by simply copy-paste the graphql/query.ql in the folder of this plugin into your graphql folder. You can also override the config inside config/laravel-wordpress.php

## How to extends the repository ?

You can replace the default WordpressRepository by changing the config/laravel-wordpress :

``php
    "repository" => MyOwnWordpressRepository::class,
``

Then you are free to implement your own class but be sure to implements the WordpressRepositoryContract interface.

`````php

use Cherrypulp\LaravelWordpress\Repositories;
use Cherrypulp\LaravelWordpress\Repositories\WordpressRepositoryContract;

class MyOwnWordpressRepository extends WordpressRepository implements WordpressRepositoryContract {

}
`````

## Security

If you discover any security related issues, please email instead of using the issue tracker.

## Credits

- [Cherry Pulp](https://github.com/cherrypulp/laravel-wordpress)
- [All contributors](https://github.com/cherrypulp/laravel-wordpress/graphs/contributors)

This package is bootstrapped with the help of
[cherrypulp/laravel-package-generator](https://github.com/cherrypulp/laravel-package-generator).
