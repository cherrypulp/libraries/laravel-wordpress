<?php

namespace Cherrypulp\LaravelWordpress\Facades;

use Cherrypulp\LaravelWordpress\Repositories\WordpressRepositoryContract;
use Illuminate\Support\Facades\Facade;

class LaravelWordpress extends Facade
{
    protected static function getFacadeAccessor()
    {
        return WordpressRepositoryContract::class;
    }
}
