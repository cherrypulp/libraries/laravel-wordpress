<?php

use Cherrypulp\LaravelWordpress\Repositories\WordpressRepositoryContract;

if (!function_exists('lwp')) {
    /**
     * @return WordpressRepositoryContract
     */
    function lwp()
    {
        return app(WordpressRepositoryContract::class);
    }
}
