<?php

namespace Cherrypulp\LaravelWordpress;

use Cherrypulp\LaravelWordpress\Repositories\WordpressRepositoryContract;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public const CONFIG_PATH = __DIR__ . '/../config/laravel-wordpress.php';

    public function boot()
    {
        $this->publishes([
            self::CONFIG_PATH => config_path('laravel-wordpress.php'),
        ], 'config');
    }

    public function register()
    {
        $this->mergeConfigFrom(
            self::CONFIG_PATH,
            'laravel-wordpress'
        );

        $this->app->bind(WordpressRepositoryContract::class, config('laravel-wordpress.repository'));

        $this->app->singleton('wp_graphql', function () {
            /**
             * @var $graphQLClient \Blok\Graphql\Graphql
             */
            $graphQLClient = config('laravel-wordpress.graphql_client');
            return new $graphQLClient(config('laravel-wordpress.graphql_url'), null);
        });

        if (!function_exists('lwp')) {
            include_once __DIR__ . '/helpers.php';
        }
    }
}
