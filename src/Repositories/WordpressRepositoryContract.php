<?php

namespace Cherrypulp\LaravelWordpress\Repositories;

use Illuminate\Contracts\Foundation\Application;

interface WordpressRepositoryContract
{
    /**
     * WordpressRepository constructor.
     * @param Application $app
     *
     * @return WordpressRepositoryContract
     */
    public function __construct(Application $app);

    /**
     * @param $uri
     * @return string
     */
    public function cleanURI($uri);

    /**
     * Get locale
     *
     * @comment ???
     * @param null $locale
     * @return mixed|string
     */
    public function getLocale($locale = null);

    /**
     * Get the categories for current language.
     *
     * @return array
     * @throws \Exception
     */
    public function getCategories($flush = false);

    /**
     * Get the category metadata
     * @throws \Exception
     */
    public function getCategoryBySlug($slug, $flush = false);

    /**
     * Get the WP highlighted posts
     *
     * @param int $limit
     * @return \Illuminate\Support\Collection|null
     */
    public function getHighlightedPosts(int $limit = 4);

    /**
     * Get home page
     * @param false $flush
     * @param null $forceLocale
     * @return mixed
     * @throws \Exception
     * @example
     * $homePage = app('wordpress')->getHome(); // return page in current locale
     * $homePage = app('wordpress')->getHome(true); // refresh cache
     * $homePage = app('wordpress')->withLocale('en_BE')->getHome(); // get a page in specified locale
     * $homePages = app('wordpress')->getHome('*'); // return an array of pages in all available locales
     */
    public function getHome(bool $flush = false, $forceLocale = null);

    /**
     * Get last updated dates.
     * @return \Illuminate\Contracts\Cache\Repository|mixed|object
     * @throws \Exception
     */
    public function getLastUpdated();

    /**
     * @param bool $flush
     * @param bool|null $forceLocale
     *
     * @return mixed
     * @throws \Exception
     */
    public function getOptions($ref, bool $flush = false, bool $forceLocale = null);

    /**
     * Get pages with given id.
     * @param $id
     * @param bool $flush
     * @param bool $forceLocale
     * @return mixed
     * @throws \Exception
     */
    public function getPageById($id, bool $flush = false, $forceLocale = null);

    /**
     * Get pages with given template name.
     * @param $template
     * @param bool $flush
     * @param bool $forceLocale
     * @return mixed
     * @throws \Exception
     */
    public function getPageByTemplate($template, bool $flush = false, $forceLocale = null);

    /**
     * Get a list of posts by category
     *
     * @param string $slug
     * @return false|\Illuminate\Support\Collection
     */
    public function getPosts($limit = 10);

    /**
     * Get a post by its slug
     *
     * @param string $slug
     * @return false|\Illuminate\Support\Collection
     */
    public function getPostBySlug(string $slug, bool $flush = false);

    /**
     * Get the content of a graphql query file
     *
     * @param String $name
     * @return false|string|null
     */
    public function getQuery(string $name);

    /**
     * Get a normalised referenced used as key for the cache.
     * @param       $name
     * @param array $args
     * @param bool $flush
     * @return string
     */
    public function getReference($name, array $args = [], $flush = false);

    /**
     * Get title and description from Wordpress.
     * @param bool $flush
     * @return mixed
     * @throws \Exception
     * @example
     * $settings = app('wordpress')->getSettings(); // return an array with "title" and "description"
     * $settings = app('wordpress')->getSettings(true); // refresh cache
     */
    public function getSettings(bool $flush = false);

    /**
     * @throws \Exception
     */
    public function reload();

    /**
     * Set last_updated timestamp for given reference or all.
     * @param null $reference
     * @throws \Exception
     */
    public function setLastUpdated($reference = null);

    /**
     * @param bool $withCache
     *
     * @return WordpressRepositoryContract
     */
    public function withCache($withCache = true);

    /**
     * @param $locale
     * @return WordpressRepositoryContract $this
     */
    public function withLocale($locale = null);
}
