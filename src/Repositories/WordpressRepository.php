<?php

namespace Cherrypulp\LaravelWordpress\Repositories;

use Carbon\Traits\Creator;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\Carbon;
use LaravelLocalization;

class WordpressRepository implements WordpressRepositoryContract
{
    private const VERSION = 'v1';
    private const CACHE_KEY = 'LWP';

    /**
     * @var \Http
     */
    private $httpClient;

    /**
     * @var null
     */
    private $locale;

    /**
     * @var Application|mixed|null
     */
    private $app;

    /**
     * @var
     */
    private $withCache;

    /**
     * @var \Corcel\Model\Attachment::class
     */
    public $attachment;

    /**
     * @var \Corcel\Model\Comment::class
     */
    public $comment;

    /**
     * @var \Corcel\Model\CustomLink::class
     */
    public $customLink;

    /**
     * @var \Corcel\Model\Menu::class
     */
    public $menu;

    /**
     * @var \Corcel\Model\MenuItem::class
     */
    public $menuItem;

    /**
     * @var \Corcel\Model\Option::class
     */
    public $option;

    /**
     * @var \Corcel\Model\Page::class
     */
    public $page;

    /**
     * @var \Corcel\Model\Post::class
     */
    public $post;

    /**
     * @var \Corcel\Model\Tag::class
     */
    public $tag;

    /**
     * @var \Corcel\Model\Taxonomy::class
     */
    public $taxonomy;

    /**
     * @var \Corcel\Model\Term::class
     */
    public $term;

    /**
     * @var \Corcel\Model\TermRelationship::class
     */
    public $termRelationship;

    /**
     * @var \Corcel\Model\User::class
     */
    public $user;

    /**
     * WordpressRepository constructor.
     * @param Application $app
     *
     * @return void
     */
    public function __construct(Application $app)
    {
        $this->app = $app;

        foreach (config('laravel-wordpress.models') as $key => $model) {
            $this->{$key} = new $model;
        }
    }

    /**
     * Return the current http client
     *
     * @param null $token
     * @param null $base_uri
     * @param \Closure|null $closure
     *
     * @return \Http|\Illuminate\Http\Client\PendingRequest
     */
    public function http($token = null, $base_uri = null, \Closure $closure  = null){

        if ($this->httpClient && !$token && !$base_uri && !$closure) {
            return $this->httpClient;
        }

        return $this->setHttp($token, $base_uri, $closure);
    }

    /**
     * Set the http client into the package
     *
     * @return \Http|\Illuminate\Http\Client\PendingRequest
     */
    public function setHttp($token = null, $base_uri = null, \Closure $closure  = null){

        $http =  \Http::withOptions([
            'base_uri' => $base_uri ?? config('laravel-wordpress.api_url')
        ]);

        if ($token = $token ?? config('laravel-wordpress.api_token')) {
            $http->withToken($token, config('laravel-wordpress.api_type', 'Bearer'));
        }

        if ($closure) {
            $http = $closure($http);
        }

        $this->httpClient = $http;

        return $http;
    }

    /**
     * Return post by id using API
     *
     * @param $id
     * @return \Illuminate\Support\Collection
     */
    public function getPostById($id){
        return $this->http()->get('wp/v2/posts/' . $id)->collect();
    }

    /**
     * @param $uri
     * @return string
     */
    public function cleanURI($uri)
    {
        $path = parse_url($uri, PHP_URL_PATH);
        $path = explode('/', $path);
        unset($path[1]);
        $path = implode('/', $path);

        if ($path === '') {
            $path = '/';
        }

        return $path;
    }

    /**
     * Get locale
     *
     * @comment ???
     * @param null $locale
     * @return mixed|string
     */
    public function getLocale($locale = null)
    {
        if (is_null($locale)) {
            $locale = $this->locale;
        }

        if (is_null($locale)) {
            $locales = config('laravel-wordpress.locales');
            return $locales[$this->app->getLocale()] ?? '*';
        }

        return $locale ?? '*';
    }

    /**
     * Get the categories for current language.
     *
     * @return array
     * @throws \Exception
     */
    public function getCategories($flush = false)
    {
        $reference = $this->getReference('categories', $flush);

        $categories = cache()->rememberForever($reference, function () {
            $query = $this->getQuery('categories');
            $response = app('wp_graphql')->query($query);

            $this->setLastUpdated();

            if (isset($response['data']['categories']['nodes'])) {
                return $response['data']['categories']['nodes'];
            }

            return [];
        });

        return $categories;
    }

    /**
     * Get the category metadata
     * @throws \Exception
     */
    public function getCategoryBySlug($slug, $flush = false)
    {
        $reference = $this->getReference('category-' . $slug);

        if ($flush || !$this->withCache) {
            cache()->forget($reference);
        }

        $category = cache()->rememberForever($reference, function () use ($slug) {
            $query = $this->getQuery('categoryBySlug');
            $response = app('wp_graphql')->query($query, [
                'name' => $slug,
            ]);

            if ($response) {
                return $response;
            }

            return [];
        });

        return $category;
    }

    /**
     * Get the WP highlighted posts
     *
     * @param int $limit
     * @return \Illuminate\Support\Collection|null
     */
    public function getHighlightedPosts(int $limit = 4)
    {
        $query = $this->getQuery('highlightedPosts');
        $response = app('wp_graphql')->query($query, [
            'limit' => $limit,
        ]);

        if (isset($response['data']['posts']['nodes'])) {
            if (!count($response['data']['posts']['nodes'])) {
                return null;
            }
            return collect($response['data']['posts']['nodes'])->map(function ($post) {
                $date = new Carbon($post['date']);
                $post['type'] = 'post';
                $post['name'] = $post['title'];
                $post['location'] = $date->format('d F Y');
                $post['image'] = isset($post['featuredImage']['node']['sourceUrl']) ? $post['featuredImage']['node']['sourceUrl'] : null;
                $post['locale'] = isset($post['locale']['locale']) ? $post['locale']['locale'] : null;
                $post['categories'] = isset($post['categories']['nodes']) ? $post['categories']['nodes'] : null;
                return $post;
            });
        }

        return null;
    }

    /**
     * Get home page
     * @param false $flush
     * @param null $forceLocale
     * @return mixed
     * @throws \Exception
     * @example
     * $homePage = app('wordpress')->getHome(); // return page in current locale
     * $homePage = app('wordpress')->getHome(true); // refresh cache
     * $homePage = app('wordpress')->withLocale('en_BE')->getHome(); // get a page in specified locale
     * $homePages = app('wordpress')->getHome('*'); // return an array of pages in all available locales
     */
    public function getHome(bool $flush = false, $forceLocale = null)
    {
        $reference = $this->getReference('home');

        if ($flush || !$this->withCache) {
            cache()->forget($reference);
        }

        $pages = cache()->rememberForever($reference, function () {
            $query = implode('', [
                $this->getQuery('fragmentPageComposer'),
                $this->getQuery('home'),
            ]);
            $response = app('wp_graphql')->query($query);

            $this->setLastUpdated();

            if (isset($response['data']['pages']['nodes'])) {
                return collect($response['data']['pages']['nodes'])->map(function ($page) {
                    if (isset($page['locale']['locale'])) {
                        $page['locale'] = $page['locale']['locale'];
                    }
                    return $page;
                });
            }

            return [];
        });

        $locale = $this->getLocale($forceLocale);

        if ($locale === '*') {
            return $pages;
        }

        return collect($pages)->firstWhere('locale', '=', $locale);
    }

    /**
     * Get last updated dates.
     * @return \Illuminate\Contracts\Cache\Repository|mixed|object
     * @throws \Exception
     */
    public function getLastUpdated()
    {
        return cache()->get($this->getReference('last_updated'));
    }

    /**
     * @param bool $flush
     * @param bool|null $forceLocale
     *
     * @return mixed
     * @throws \Exception
     */
    public function getOptions($ref, bool $flush = false, bool $forceLocale = null)
    {
        $reference = $this->getReference('options');

        if ($flush || !$this->withCache) {
            cache()->forget($reference);
        }

        $options = cache()->rememberForever($reference, function () use ($reference, $ref) {
            $query = $this->getQuery('options');
            $response = app('wp_graphql')->query($query);

            $this->setLastUpdated($reference);

            if (isset($response['data'][$ref]['allLanguages'])) {
                $data = json_decode($response['data'][$ref]['allLanguages'], true);

                foreach ($data as $key => $language) {
                    foreach (['menu_event', 'menu_destination', 'menu_services'] as $name) {
                        if (isset($language[$name]['menu_pages'])) {
                            foreach ($language[$name]['menu_pages'] as $k => $page) {
                                if (isset($page['post_title'])) {
                                    $data[$key][$name]['menu_pages'][$k]['title'] = $page['post_title'];
                                }

                                if (isset($page['url'])) {
                                    $data[$key][$name]['menu_pages'][$k]['url'] = LaravelLocalization::localizeURL(app('wordpress')->cleanURI($page['url']));
                                }

                                if (isset($page['thumbnail']) && count($page['thumbnail'])) {
                                    $data[$key][$name]['menu_pages'][$k]['image'] = $page['thumbnail'][0];
                                } else {
                                    $data[$key][$name]['menu_pages'][$k]['image'] = asset('images/image-default.png');
                                }

                            }
                        }
                    }
                }

                return $data;
            }

            return [];
        });

        $locale = $this->getLocale($forceLocale);

        if ($locale === '*') {
            return $options;
        }

        return collect($options)->firstWhere('locale', '=', $locale);
    }

    /**
     * Get pages with given id.
     * @param $id
     * @param bool $flush
     * @param bool $forceLocale
     * @return mixed
     * @throws \Exception
     */
    public function getPageById($id, bool $flush = false, $forceLocale = null)
    {
        $reference = $this->getReference('page', [$id]);

        if ($flush || !$this->withCache) {
            cache()->forget($reference);
        }

        return cache()->rememberForever($reference, function () use ($reference, $id) {
            $query = implode('', [
                $this->getQuery('fragmentPageComposer'),
                $this->getQuery('pageById'),
            ]);
            $response = app('wp_graphql')->query($query, ['id' => $id]);

            $this->setLastUpdated($reference);

            if (isset($response['data']['page'])) {
                if (isset($response['data']['page']['locale']['locale'])) {
                    $response['data']['page']['locale'] = $response['data']['page']['locale']['locale'];
                }

                return $response['data']['page'];
            }

            return [];
        });
    }

    /**
     * Get pages with given template name.
     * @param $template
     * @param bool $flush
     * @param bool $forceLocale
     * @return mixed
     * @throws \Exception
     */
    public function getPageByTemplate($template, bool $flush = false, $forceLocale = null)
    {
        $reference = $this->getReference('page', [$template]);

        if ($flush || !$this->withCache) {
            cache()->forget($reference);
        }

        $pages = cache()->rememberForever($reference, function () use ($reference, $template) {
            $query = implode('', [
                $this->getQuery('fragmentPageComposer'),
                $this->getQuery('pageByTemplate'),
            ]);
            $response = app('wp_graphql')->query($query, [
                'template' => $template,
            ]);

            $this->setLastUpdated($reference);

            if (isset($response['data']['pages']['nodes'])) {
                return collect($response['data']['pages']['nodes'])->map(function ($page) {
                    if (isset($page['locale']['locale'])) {
                        $page['locale'] = $page['locale']['locale'];
                    }
                    return $page;
                })->toArray();
            }

            return [];
        });

        $locale = $this->getLocale($forceLocale);

        if ($locale === '*') {
            return $pages;
        }

        return collect($pages)->firstWhere('locale', '=', $locale);
    }

    /**
     * Get a list of posts by category
     *
     * @param string $slug
     * @return false|\Illuminate\Support\Collection
     */
    public function getPosts($limit = 10)
    {
        $query = $this->getQuery('posts');

        $response = app('wp_graphql')->query($query, [
            'limit' => $limit,
        ]);

        if (isset($response['data']['posts']['nodes'])) {
            if (!count($response['data']['posts']['nodes'])) {
                return null;
            }
            return collect($response['data']['posts']['nodes'])->map(function ($post) {
                $post['image'] = isset($post['featuredImage']['node']['sourceUrl']) ? $post['featuredImage']['node']['sourceUrl'] : null;
                $post['locale'] = isset($post['locale']['locale']) ? $post['locale']['locale'] : null;
                $post['categories'] = isset($post['categories']['nodes']) ? $post['categories']['nodes'] : null;
                return $post;
            });
        }

        return null;
    }

    /**
     * Get a post by its slug
     *
     * @param string $slug
     * @return false|\Illuminate\Support\Collection
     */
    public function getPostBySlug(string $slug, bool $flush = false)
    {
        $query = $this->getQuery('postBySlug');

        $response = app('wp_graphql')->query($query, ['slug' => $slug]);

        if (isset($response['data']['posts']['nodes'])) {
            if (!count($response['data']['posts']['nodes'])) {
                return null;
            }
            return $response['data']['posts']['nodes'][0];
        }

        return null;
    }

    /**
     * Get the content of a graphql query file
     *
     * @param String $name
     * @return false|string|null
     */
    public function getQuery(string $name)
    {
        $path = config('laravel-wordpress.graphql_path') . '/' . $name . '.graphql';

        if (!file_exists($path)) {
            $path = __DIR__ . '/../../graphql/' . $name . '.graphql';
        }

        if (file_exists($path)) {
            return file_get_contents($path);
        }

        return null;
    }

    /**
     * Get a normalised referenced used as key for the cache.
     * @param       $name
     * @param array $args
     * @param bool $flush
     * @return string
     */
    public function getReference($name, array $args = [], $flush = false)
    {
        $ref = self::CACHE_KEY.'_'.$name . '_' . implode('_', $args) . '_' . self::VERSION;

        if ($flush) {
            $this->app->get('cache')->forget($ref);
        }

        // Register cache ref
        $cachedRef = $this->getCachedRef();

        $cachedRef[$ref] = $ref;

        cache()->forever(self::CACHE_KEY, $cachedRef);

        return $ref;
    }

    /**
     * Get title and description from Wordpress.
     * @param bool $flush
     * @return mixed
     * @throws \Exception
     * @example
     * $settings = app('wordpress')->getSettings(); // return an array with "title" and "description"
     * $settings = app('wordpress')->getSettings(true); // refresh cache
     */
    public function getSettings(bool $flush = false)
    {
        $reference = $this->getReference('settings', [], $flush);

        return cache()->rememberForever($reference, function () use ($reference) {

            $query = $this->getQuery('settings');
            $response = app('wp_graphql')->query($query);

            $this->setLastUpdated($reference);

            if (isset($response['data']['generalSettings'])) {
                return $response['data']['generalSettings'];
            }

            return [];
        });
    }

    /**
     * Get the cached ref
     *
     * @return Application|mixed|null
     */
    public function getCachedRef(){
        return $this->app->get('cache')->rememberForever(self::CACHE_KEY, function () {
            return [];
        });
    }

    /**
     * Return graphql instance
     *
     * @return \Blok\Graphql\Graphql
     */
    public function graphql(){
        return app('wp_graphql');
    }

    /**
     * @throws \Exception
     */
    public function reload()
    {
        $lastUpdated = $this->getLastUpdated();

        if (is_array($lastUpdated)) {
            // invalidate all cached queries
            if ($lastUpdated && count($lastUpdated)) {
                foreach (array_merge($lastUpdated, [$this->getReference('last_updated')]) as $key) {
                    cache()->forget($key);
                }
            }
        }

        $this->getSettings(true);
        $this->getHome(true);
    }

    /**
     * Flush all queries cached
     *
     * @return self
     */
    public function flush(){

        foreach ($this->getCachedRef() as $key) {
            cache()->forget($key);
        }

        return $this;
    }

    /**
     * Set last_updated timestamp for given reference or all.
     * @param null $reference
     * @throws \Exception
     */
    public function setLastUpdated($reference = null)
    {
        $lastUpdated = cache()->get($this->getReference('last_updated')) ?? [];
        $timestamp = date('Y-m-d H:i:s');

        if ($reference) {
            $lastUpdated[$reference] = $timestamp;
        } else {
            foreach ($lastUpdated as $key => $updated) {
                $lastUpdated[$key] = $timestamp;
            }
        }

        cache()->put($this->getReference('last_updated'), $lastUpdated);
    }

    /**
     * @param bool $withCache
     *
     * @return WordpressRepositoryContract
     */
    public function withCache($withCache = true)
    {
        $this->withCache = $withCache;
        return $this;
    }

    /**
     * @param $locale
     * @return WordpressRepositoryContract $this
     */
    public function withLocale($locale = null)
    {
        $this->locale = $locale ?? '*';
        return $this;
    }
}
