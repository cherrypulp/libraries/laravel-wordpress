<?php

use Cherrypulp\LaravelWordpress\Repositories\WordpressRepository;

return [
    'locales' => [
        'fr' => 'fr_FR',
        'en' => 'en_US',
    ],
    "repository" => WordpressRepository::class,
    "api_url" => env('WP_API_URL'),
    "api_token" => env('WP_API_TOKEN'),
    "api_type" => env('WP_API_TYPE', 'Bearer'),
    "graphql_client" => \Blok\Graphql\Graphql::class,
    "graphql_url" => env('WP_GRAPHQL_URL'),
    "graphql_path" => base_path('graphql'),
    "models" => [
        'attachment' => Corcel\Model\Attachment::class,
        'comment' => Corcel\Model\Comment::class,
        'customLink' => Corcel\Model\CustomLink::class,
        'menu' => Corcel\Model\Menu::class,
        'menuItem' => Corcel\Model\MenuItem::class,
        'option' => Corcel\Model\Option::class,
        'page' => Corcel\Model\Page::class,
        'post' => Corcel\Model\Post::class,
        'tag' => Corcel\Model\Tag::class,
        'taxonomy' => Corcel\Model\Taxonomy::class,
        'term' => Corcel\Model\Term::class,
        'termRelationship' => Corcel\Model\TermRelationship::class,
        'user' => Corcel\Model\User::class,
    ],
    'cache_strategy' => env('WP_CACHE_TYPE', 'nocache') //nocache:slower|last_edit:quicker|cached:fastest
];
